# Android Application

**Description:** This component incorporates the client and sub-manager of the system implementation

**Project:** Inconnu

**Position on FIWARE architecture:** IoT client device

***

## Contents 
* Compilation

### Compilation
To compile this component make sure to have the following requirements:

* Android Studio IDE installed
* Android device with debugging enabled and connected to the computer
* Android device version >= 26 (Oreo)

To compile and install execute this:
```bash
#Maven Install Project Dependencies

#Install pTASC
git clone https://Iqop20@bitbucket.org/Iqop20/ptasc.git && cd ptasc &
& mvn clean install

#Install Anonymization Core
git clone https://Iqop20@bitbucket.org/Iqop20/anonymization-core.git core && cd core && mvn clean install

#Compile and install APK
./gradlew installDebug
```


***
For more information about the usage of the application check the dissertation implementation section, where the workings of each Activity are described