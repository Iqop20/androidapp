## Data collector APP
Before running or installing make sure to do the following:

```bash
git clone https://Iqop20@bitbucket.org/Iqop20/ptasc.git
cd ptasc
mvn clean install
```

This will install the ptasc dependencies needed to compile the android app