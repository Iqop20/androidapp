package inconnu.android.datacollector;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import inconnu.android.datacollector.fragments.LocationFragment;
import inconnu.anonymization.core.databases.dynamic.utils.GeoLocationPoint;

public class Utils {
    public static JSONObject generateRequestBody(GeoLocationPoint point,long timestamp, long heartbeat){

        try {
            JSONObject lat = generateDoubleJSON(point.latitude);
            JSONObject lon = generateDoubleJSON(point.longitude);
            JSONObject alt = generateDoubleJSON(point.altitude);
            JSONObject time = generateLongJSON(timestamp);
            JSONObject heart = generateLongJSON(heartbeat);

            JSONObject toRet = new JSONObject();
            toRet.put("latitude",lat);
            toRet.put("longitude",lon);
            toRet.put("altitude",alt);
            toRet.put("timestamp",time);
            toRet.put("hearbeat",heart);

            return toRet;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }



    public static JSONObject updateEntity(String name,String pool,GeoLocationPoint point,long timestamp,long heartbeat){
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("url", "/v2/entities/" + pool + "-" + name +((LocationFragment.isPublic)?("::public"):("")) + "/attrs?options=forcedUpdate");
            jsonObject.put("method", "PATCH");
            jsonObject.put("body", Objects.requireNonNull(generateRequestBody(point, timestamp, heartbeat)).toString());

            return jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }





    public static JSONObject registerEntity(String name,String pool){
        try{

            JSONObject body = new JSONObject();
            body.put("id",pool+"-"+name+((LocationFragment.isPublic)?("::public"):("")));
            body.put("type","LHT"); //Location+Heartbeart+Timestamp
            body.put("latitude",generateDoubleJSON(0));
            body.put("longitude",generateDoubleJSON(0));
            body.put("altitude",generateDoubleJSON(0));
            body.put("timestamp",generateLongJSON(System.currentTimeMillis()));
            body.put("hearbeat",generateLongJSON(0));


            JSONObject request = new JSONObject();
            request.put("url","/v2/entities");
            request.put("method","POST");
            request.put("body",body.toString());

            return request;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }



    static JSONObject generateDoubleJSON(double value) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value",value);
        jsonObject.put("type","double");

        return jsonObject;
    }

    static JSONObject generateLongJSON(long value) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value",value);
        jsonObject.put("type","long");

        return jsonObject;
    }

    static JSONObject generateStringJSON(String value) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value",value);
        jsonObject.put("type","string");

        return jsonObject;
    }

    public static boolean isInternetConnected(){
        try {
            String command = "ping -c 1 google.com";
            return (Runtime.getRuntime().exec(command).waitFor() == 0);
        } catch (Exception e) {
            return false;
        }
    }
}
