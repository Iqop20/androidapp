package inconnu.android.datacollector;

import android.app.Activity;
import android.widget.Toast;

import inconnu.ptasc.Configuration;
import inconnu.ptasc.MessageHandler;

public class AndroidMessageHandler extends MessageHandler {
    Activity context;
    public AndroidMessageHandler(Activity context) {
        super();
        this.context = context;
    }

    @Override
    public void error(Exception e, String message) {
        if (e!=null) {
            System.out.println(Configuration.PTASC_LOG +"ERROR"+ message + ", due to: " + e.getMessage());
            if (context!=null) {
                context.runOnUiThread(() -> {
                    Toast.makeText(context, "ERROR" + message + ",due to " + e.getMessage(), Toast.LENGTH_SHORT).show();
                });
            }
        }else{
            System.out.println(Configuration.PTASC_LOG+"ERROR"+ message);
            if (context!=null) {
                context.runOnUiThread(() -> {
                    Toast.makeText(context, "ERROR" + message, Toast.LENGTH_SHORT).show();
                });
            }
        }
    }

    @Override
    public void message(String message) {
//        System.out.println(Configuration.PTASC_LOG+message);
//        context.runOnUiThread(()->{
//            Toast.makeText(context, message,Toast.LENGTH_SHORT).show();
//        });
    }
}
