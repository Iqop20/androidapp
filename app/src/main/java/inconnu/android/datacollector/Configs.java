package inconnu.android.datacollector;
public class Configs {
    public static int ORION_PORT=443;
    public static String ORION_PROTOCOL = "https://";
    public static String ORION_SERVER = "test.qop.pt";

    public static String ORION_URL = ORION_PROTOCOL+ORION_SERVER+":"+ORION_PORT+"/v2/";

}
