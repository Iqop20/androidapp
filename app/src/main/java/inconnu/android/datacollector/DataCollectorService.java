package inconnu.android.datacollector;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import org.json.JSONObject;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import inconnu.android.datacollector.fragments.LocationFragment;
import inconnu.anonymization.core.databases.dynamic.GeoIndistinguishabilityLaplaceAdaptativeEpsilon;
import inconnu.anonymization.core.databases.dynamic.GeoIndistinguishabilityLaplaceCluster;
import inconnu.anonymization.core.databases.dynamic.GeoIndistinguishabilityLaplaceSimple;
import inconnu.anonymization.core.databases.dynamic.utils.GeoLocationPoint;
import inconnu.ptasc.Configuration;
import inconnu.ptasc.PTascNode;
import inconnu.ptasc.communication.processors.OrionProcessorPTASC;

public class DataCollectorService extends Service {
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    String currentStatus=null;
    PowerManager.WakeLock inconnuDataCollectorWakeLock;
    GeoLocationPoint currentPoint;
    long timestamp;
    final int UPDATE_INTERVAL_SECONDS =LocationFragment.frequency; //5 minutes
    Semaphore locationAvailable = new Semaphore(1,true);
    boolean uploaderRun=true;
    ScheduledFuture<?> uploader;
    private Context appContext;
    LocationManager locationManager=null;
    LocationListener locationListener=null;

    AtomicInteger numberCollected=new AtomicInteger(0);
    AtomicInteger numberFailed = new AtomicInteger(0);
    AtomicInteger numberExceptions= new AtomicInteger(0);

    String statusdetail="";
    PTascNode node;

    NotificationManager nmager;

    String cityPool;


    FusedLocationProviderClient mFusedLocationClient;
    LocationCallback mLocationCallback;


    static int selectedOption= LocationFragment.selectedOption;
    static GeoIndistinguishabilityLaplaceSimple geoIndistinguishabilityLaplaceSimple = LocationFragment.geoIndistinguishabilityLaplaceSimple;
    static GeoIndistinguishabilityLaplaceCluster geoIndistinguishabilityLaplaceCluster = LocationFragment.geoIndistinguishabilityLaplaceCluster;
    static GeoIndistinguishabilityLaplaceAdaptativeEpsilon geoIndistinguishabilityLaplaceAdaptativeEpsilon = LocationFragment.geoIndistinguishabilityLaplaceAdaptativeEpsilon;


    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void createNotificationChannel() {
         inconnuDataCollectorWakeLock = ((PowerManager) getSystemService(Context.POWER_SERVICE)).newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "DataCollector:WakeLock");
            inconnuDataCollectorWakeLock.acquire();


            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            nmager = getSystemService(NotificationManager.class);
            nmager.createNotificationChannel(serviceChannel);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        appContext = getBaseContext();        //Get the context here


        cityPool = MasterActivity.cityPool;
        node = MasterActivity.node;

        createNotificationChannel();


        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentIntent(pendingIntent)
                .setContentTitle("Data Collector")
                .setContentText("Data Collector is starting")
                .setSmallIcon(R.drawable.ic_notifications)
                .setOnlyAlertOnce(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .build();

        startForeground(1, notification);



        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            mySelfStop();
            return START_NOT_STICKY;
        }

        /*
            Launch service
        */

        try {
            locationAvailable.acquire();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

         System.out.println("Using fused location");
            LocationRequest mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval((UPDATE_INTERVAL_SECONDS/5)*1000);
            mLocationRequest.setFastestInterval(1000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);


            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);

                    System.out.println(Configuration.PTASC_LOG + "DATA location received");
                    Location lastLocation = locationResult.getLastLocation();
                    currentPoint = new GeoLocationPoint(lastLocation.getLatitude(),lastLocation.getLongitude());


                    if (locationAvailable.availablePermits()==0) locationAvailable.release();

                }
            };


            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.getMainLooper());


        new Thread(()-> {
            boolean noConn=false;
                ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                if (connectivityManager.getActiveNetworkInfo()!=null) {
                    if (connectivityManager.getActiveNetworkInfo().isConnected() && Utils.isInternetConnected()) {
                        boolean failed=false;

                        if (node.communicate("endpoint." + cityPool, Configuration.DEVICE_COMMUNICATION_PORT, OrionProcessorPTASC.processorName, Utils.registerEntity(node.name,node.pool).toString().getBytes()) ==null) {
                                    mySelfStop();
                                    failed = true;
                        }
                        if (!failed) {
                            Notification n = new NotificationCompat.Builder(this, CHANNEL_ID)
                                    .setContentIntent(pendingIntent)
                                    .setContentTitle("Data Collector will collect data every ~" + (UPDATE_INTERVAL_SECONDS / 60) + " minutes")
                                    .setContentText("Device registered on ORION with id: " + node.name + "." + node.pool)
                                    .setSmallIcon(R.drawable.ic_notifications)
                                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                                    .build();
                            if (nmager != null) nmager.notify(1, n);

                        }
                    }else noConn = true;
                } else noConn=true;

                if (noConn) {
                    Notification n = new NotificationCompat.Builder(this, CHANNEL_ID)
                            .setContentIntent(pendingIntent)
                            .setContentTitle("Data Collector failed initializing")
                            .setContentText("No internet connection, exiting")
                            .setSmallIcon(R.drawable.ic_notification_fail)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .build();

                    if(appContext!=null) {
                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(() -> Toast.makeText(appContext, "Service stopped due to no internet connection", Toast.LENGTH_LONG).show());
                    }

                    if (nmager != null) nmager.notify(1, n);
                    mySelfStop();
                }
        }).start();


        uploader = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            System.out.println(Configuration.PTASC_LOG+"HERE");
            timestamp = System.currentTimeMillis();
            if (uploaderRun) {
                try {
                    locationAvailable.tryAcquire(30, TimeUnit.SECONDS);

                    GeoLocationPoint sanitized=null;

                    switch (selectedOption){
                        case 0:
                            sanitized = geoIndistinguishabilityLaplaceSimple.sanitizePoint(currentPoint);
                            break;
                        case 1:
                            sanitized = geoIndistinguishabilityLaplaceCluster.sanitizeNewPoint(currentPoint);
                            break;
                        case 2:
                            sanitized = geoIndistinguishabilityLaplaceAdaptativeEpsilon.sanitizeNewPoint(currentPoint);
                    }


                    if (sanitized!=null) {

                        System.out.println(Configuration.PTASC_LOG + "DATA CURRENT: long:" + currentPoint.longitude + ", lat:" + currentPoint.latitude + ", time: " + timestamp);


                        System.out.println(Configuration.PTASC_LOG + "DATA SANITIZED: long:" + sanitized.longitude + ", lat:" + sanitized.latitude + ", time: " + timestamp);


                        JSONObject commsRes = null;

                        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

                        boolean noConnect = false;

                        if (connectivityManager.getActiveNetworkInfo() != null) {
                            if (connectivityManager.getActiveNetworkInfo().isConnected() && Utils.isInternetConnected()) {

                                        System.out.println(Configuration.PTASC_LOG + "DATA city pool: " + cityPool);
                                        byte[] cResponse = node.communicate("endpoint." + cityPool, Configuration.DEVICE_COMMUNICATION_PORT, OrionProcessorPTASC.processorName, Utils.updateEntity(node.name, node.pool, sanitized, timestamp, -1).toString().getBytes());
                                        try {
                                            commsRes = new JSONObject(new String(cResponse));
                                        }catch (Exception e){
                                            System.out.println(e);
                                            commsRes=null;
                                        }

                                if (commsRes != null) {

                                    String data = commsRes.getString("data");
                                    int code = commsRes.getInt("code");

                                    System.out.println(Configuration.PTASC_LOG + "DATA RESPONSE: " + data);
                                    System.out.println(Configuration.PTASC_LOG + "DATA RESPONSE CODE: " + code);

                                    if (code >= 200 && code < 300) {
                                        numberCollected.set(numberCollected.get() + 1);
                                        statusdetail = "Success";
                                    } else {
                                        numberFailed.set(numberFailed.get() + 1);
                                        statusdetail = "Orion responded with error";
                                    }
                                    currentStatus = Integer.toString(code);
                                    Notification n = new NotificationCompat.Builder(this, CHANNEL_ID)
                                            .setContentIntent(pendingIntent)
                                            .setOnlyAlertOnce(true)
                                            .setContentTitle("Data Collector running: " + numberCollected.get() + " entry(ies) collected")
                                            .setStyle(new NotificationCompat.BigTextStyle()
                                                    .setSummaryText("Status: " + currentStatus)
                                            )
                                            .setSmallIcon(R.drawable.ic_notifications)
                                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                                            .build();
                                    if (nmager != null) nmager.notify(1, n);
                                } else {
                                    numberFailed.set(numberFailed.get() + 1);
                                    currentStatus = "null response";
                                    statusdetail = "Problem with the communication with the endpoint";
                                    Notification n = new NotificationCompat.Builder(this, CHANNEL_ID)
                                            .setContentIntent(pendingIntent)
                                            .setOnlyAlertOnce(true)
                                            .setContentTitle("Data Collector running: " + numberCollected.get() + " entry(ies) collected")
                                            .setStyle(new NotificationCompat.BigTextStyle()
                                                    .setSummaryText("Status: " + currentStatus)
                                            )
                                            .setSmallIcon(R.drawable.ic_notifications)
                                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                                            .build();
                                    if (nmager != null) nmager.notify(1, n);
                                }
                            } else noConnect = true;
                        } else noConnect = true;

                        if (noConnect) {
                            numberFailed.set(numberFailed.get() + 1);
                            currentStatus = "No internet connection";
                            statusdetail = "No internet connection checking again in " + (UPDATE_INTERVAL_SECONDS / 60) + " minutes";
                            Notification n = new NotificationCompat.Builder(this, CHANNEL_ID)
                                    .setContentIntent(pendingIntent)
                                    .setContentTitle("Data Collector running: " + numberCollected.get() + " entry(ies) collected")
                                    .setStyle(new NotificationCompat.BigTextStyle()
                                            .setSummaryText("Status: " + currentStatus))
                                    .setSmallIcon(R.drawable.ic_notification_fail)
                                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                                    .build();
                            if (nmager != null) nmager.notify(1, n);
                        }
                    }else{
                        Notification n = new NotificationCompat.Builder(this, CHANNEL_ID)
                                .setContentIntent(pendingIntent)
                                .setContentTitle("Error on sanitization")
                                .setContentText("Exiting")
                                .setSmallIcon(R.drawable.ic_notification_fail)
                                .setPriority(NotificationCompat.PRIORITY_HIGH)
                                .build();
                        if (nmager != null) nmager.notify(1, n);
                        mySelfStop();
                    }
                } catch (Exception e) {
                    numberExceptions.set(numberExceptions.get()+1);
                    e.printStackTrace();
                    currentStatus="Exception occurred. Trying again in ~"+(UPDATE_INTERVAL_SECONDS/60)+" minutes";
                    statusdetail=e.getMessage();
                    Notification n = new NotificationCompat.Builder(this, CHANNEL_ID)
                            .setContentIntent(pendingIntent)
                            .setOnlyAlertOnce(true)
                            .setContentTitle("Data Collector running: " + numberCollected.get() + " entry(ies) collected")
                            .setStyle(new NotificationCompat.BigTextStyle()
                                    .setSummaryText("Status: " + currentStatus))
                            .setSmallIcon(R.drawable.ic_notification_fail)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .build();
                    if (nmager != null) nmager.notify(1, n);
                    System.out.println(Configuration.PTASC_LOG + "DATA uploader thread exception: "+e.getMessage());
                }
            }

        }, UPDATE_INTERVAL_SECONDS, UPDATE_INTERVAL_SECONDS, TimeUnit.SECONDS);

        return START_NOT_STICKY;

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (inconnuDataCollectorWakeLock!=null) inconnuDataCollectorWakeLock.release();

        System.out.println(Configuration.PTASC_LOG+"DATA onDestroy called");
        uploaderRun=false;
        uploader.cancel(false);
        if (mFusedLocationClient!=null)mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        if (locationManager!=null)locationManager.removeUpdates(locationListener);
    }


    public void mySelfStop(){
        stopSelf();
    }




    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
