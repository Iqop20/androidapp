package inconnu.android.datacollector;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.yubico.yubikit.android.ui.OtpActivity;
import org.apache.commons.lang3.RandomStringUtils;
import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.bouncycastle.crypto.util.SubjectPublicKeyInfoFactory;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.File;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.concurrent.ExecutionException;

import inconnu.ptasc.client.Client;

public class ClientActivity extends AppCompatActivity {

    PublicKey CAECDSApubKey;
    ECPublicKeyParameters CAECIESpubKey;
    String name;
    String pool;
    String cityPool;
    Client client;


    final int OTP_REQUEST_CODE=1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewsEnabler(true);
        SharedPreferences clientPreferences = getSharedPreferences("client", MODE_PRIVATE);

        if (clientPreferences.contains("name")){
            try {
                CAECIESpubKey = (ECPublicKeyParameters) PublicKeyFactory.createKey(Base64.decode(clientPreferences.getString("CAECIES", null), Base64.NO_WRAP));
                CAECDSApubKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(Base64.decode(clientPreferences.getString("CAECDSA", null), Base64.NO_WRAP)));
                name = clientPreferences.getString("name","");
                pool = clientPreferences.getString("pool","");
                cityPool = clientPreferences.getString("city","");
                ((TextView) findViewById(R.id.randomId)).setText(name);
                ((EditText) findViewById(R.id.pool)).setText(pool);
                ((EditText) findViewById(R.id.citypool)).setText(cityPool);
                viewsEnabler(false);
                client = new ClientUtils.LaunchClient().execute(this,name,CAECIESpubKey,CAECDSApubKey,0,pool,cityPool).get();
            } catch (NoSuchAlgorithmException | IOException | InvalidKeySpecException e) {
                reset(null);
                e.printStackTrace();
                System.exit(1);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    public void provision(View view){

        pool = ((EditText) findViewById(R.id.pool)).getText().toString();

        if (pool.equals("")){
            Toast.makeText(this,"Bad pool name",Toast.LENGTH_SHORT).show();
            return;
        }

        cityPool = ((EditText)findViewById(R.id.citypool)).getText().toString();

        if (cityPool.equals("")){
            Toast.makeText(this,"Bad city pool name",Toast.LENGTH_SHORT).show();
            return;
        }

        getSharedPreferences("client", MODE_PRIVATE).edit().putString("city",cityPool).apply();

        name = RandomStringUtils.random(24,true,false);

        ((TextView) findViewById(R.id.randomId)).setText(name);

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("Scan the QR code containing "+pool+" manager keys");
        integrator.initiateScan();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case OTP_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String otp = data.getStringExtra(OtpActivity.EXTRA_OTP);

                    try {
                        client = new ClientUtils.LaunchClient().execute(this,name,CAECIESpubKey,CAECDSApubKey,otp,pool,cityPool).get();
                        if (client!=null) {
                            client.stopServices();
                            /*
                            Update shared preferences
                            */
                            SharedPreferences.Editor preferenceEditor = getSharedPreferences("client", MODE_PRIVATE).edit();
                            preferenceEditor.putString("name",name);
                            preferenceEditor.putString("pool",pool);
                            preferenceEditor.putString("CAECIES",Base64.encodeToString(SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(CAECIESpubKey).getEncoded(),Base64.NO_WRAP));
                            preferenceEditor.putString("CAECDSA",Base64.encodeToString(CAECDSApubKey.getEncoded(),Base64.NO_WRAP));
                            preferenceEditor.apply();
                            viewsEnabler(false);

                        }else throw new Exception("Failed to get client");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (resultCode != Activity.RESULT_CANCELED && data != null) {
                    Throwable error = (Throwable)data.getSerializableExtra(OtpActivity.EXTRA_ERROR);
                    assert error != null;
                    Toast.makeText(this,error.getMessage(),Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

                if (result.getContents()!=null){
                    try {
                        JSONObject keys = new JSONObject(result.getContents());

                        CAECIESpubKey = (ECPublicKeyParameters) PublicKeyFactory.createKey(Base64.decode(keys.getString("CAECIES"),Base64.NO_WRAP));
                        CAECDSApubKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(Base64.decode(keys.getString("CAECDSA"),Base64.NO_WRAP)));

                        startActivityForResult(new Intent(this, OtpActivity.class), OTP_REQUEST_CODE);
                    } catch (JSONException | IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                        (new AndroidMessageHandler(this)).error(e,"Error processing QR code");
                    }
                }else (new AndroidMessageHandler(this)).error(null,"Error processing QR code due to lack of information");
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (client!=null) client.stopServices();
    }

    public void reset(View v){
        SharedPreferences mainActivity = getSharedPreferences("mainActivity", MODE_PRIVATE);
        mainActivity.edit().remove("mode").apply();

        getSharedPreferences("client",MODE_PRIVATE).edit().clear().apply();

        new File(getFilesDir(),name+"Certificate.ptasc").delete();
        new File(getFilesDir(),name+"ECIESPubKey.ptasc").delete();
        new File(getFilesDir(),name+"ECIESPrivKey.ptasc").delete();
        new File(getFilesDir(),name+"CertificatePublicKey.ptasc").delete();
        new File(getFilesDir(),name+"CertificatePrivKey.ptasc").delete();


        if (client!=null) client.stopServices();

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void viewsEnabler(boolean state){
        findViewById(R.id.pool).setEnabled(state);
        findViewById(R.id.provision).setEnabled(state);
        findViewById(R.id.citypool).setEnabled(state);
    }



    public void master(View v){
        Intent i = new Intent(this,MasterActivity.class);
        MasterActivity.node = client;
        MasterActivity.cityPool = cityPool;
        startActivity(i);
    }


}
