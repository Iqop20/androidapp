package inconnu.android.datacollector;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.yubico.yubikit.android.ui.OtpActivity;

import org.bouncycastle.crypto.params.ECPublicKeyParameters;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateEncodingException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import inconnu.ptasc.Configuration;
import inconnu.ptasc.communication.processors.UpdateProcessorPTASC;
import inconnu.ptasc.manager.Manager;

public class ManagerActivity extends AppCompatActivity {

    String managedPool;
    static Manager manager;
    final int OTP_REQUEST_CODE=1;
    ECPublicKeyParameters CAECIESpubKey;
    PublicKey CAECDSApubKey;
    String poolToBeAppended;
    static String cityPool;




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager);
        Configuration.TEMP_PATH = getFilesDir();

        appendingViewsEnabling(false);
        startViewsEnabling(true);

        SharedPreferences managerPreferences = getSharedPreferences("manager", MODE_PRIVATE);
        if (managerPreferences.contains("configured")) {
            managedPool = managerPreferences.getString("managedPool", null);
            cityPool = managerPreferences.getString("cityD",null);

            File certificateFile = new File(getFilesDir(), "managerCertificate.ptasc");
            File ECIESpub = new File(getFilesDir(), "managerECIESPubKey.ptasc");
            File ECIESpriv = new File(getFilesDir(), "managerECIESPrivKey.ptasc");
            File certificatePublicKeyFile = new File(getFilesDir(), "managerCertificatePublicKey.ptasc");
            File certificatePrivKeyFile = new File(getFilesDir(), "managerCertificatePrivKey.ptasc");
            File yubicoOtp = new File(getFilesDir(),"yubico.ptasc");

            try {
                yubicoOtp.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this,"Failed to create yubico.ptasc file",Toast.LENGTH_LONG).show();
                return;
            }

            try {
                    manager = new Manager(managedPool, certificatePublicKeyFile, certificatePrivKeyFile, certificateFile, ECIESpub, ECIESpriv, null, new AndroidMessageHandler(this),null,null,"endpoint."+cityPool,true,yubicoOtp);
                    manager.endpointUSSN = "endpoint."+cityPool;
                    manager.addCommunicationProcessor(new UpdateProcessorPTASC());
                startViewsEnabling(false);
                appendingViewsEnabling(true);


                findViewById(R.id.qrCode).setVisibility(View.VISIBLE);

                Bitmap bmp = ManagerUtils.generateKeysQRCode(manager);
                if (bmp!=null) ((ImageView)findViewById(R.id.qrCodeImage)).setImageBitmap(bmp);


            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (managerPreferences.contains("appended")){
            cityPool = getSharedPreferences("manager", MODE_PRIVATE).getString("appended",null);
            appendingViewsEnabling(false);
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (manager!=null) manager.stopServices();
    }


    public void start(View v){
        managedPool = ((EditText) findViewById(R.id.poolManage)).getText().toString();
        String yubicoID = ((EditText)findViewById(R.id.otpID)).getText().toString();
        String yubicoSecret = ((EditText) findViewById(R.id.otpSecret)).getText().toString();
        cityPool = ((EditText) findViewById(R.id.cityD)).getText().toString();
        File certificateFile = new File(getFilesDir(),"managerCertificate.ptasc");
        File ECIESpub = new File(getFilesDir(),"managerECIESPubKey.ptasc");
        File ECIESpriv = new File(getFilesDir(),"managerECIESPrivKey.ptasc");
        File certificatePublicKeyFile = new File(getFilesDir(),"managerCertificatePublicKey.ptasc");
        File certificatePrivKeyFile = new File(getFilesDir(),"managerCertificatePrivKey.ptasc");
        File yubicoOtp = new File(getFilesDir(),"yubico.ptasc");
        try {
            yubicoOtp.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this,"Failed to create yubico.ptasc file",Toast.LENGTH_LONG).show();
            return;
        }


        try {

            manager = new Manager(managedPool,certificatePublicKeyFile,certificatePrivKeyFile,certificateFile,ECIESpub,ECIESpriv,null,new AndroidMessageHandler(this),null,null,"endpoint."+cityPool,true,yubicoOtp);
            manager.endpointUSSN = "endpoint."+cityPool;
            manager.addToken(yubicoID,yubicoSecret);
            manager.addCommunicationProcessor(new UpdateProcessorPTASC());

            SharedPreferences managerSharedPref = getSharedPreferences("manager", MODE_PRIVATE);
            SharedPreferences.Editor edit = managerSharedPref.edit();
            edit.putString("configured","true");
            edit.putString("managedPool",managedPool);
            edit.putString("cityD",cityPool);
            edit.apply();
            appendingViewsEnabling(true);
            startViewsEnabling(false);




            findViewById(R.id.qrCode).setVisibility(View.VISIBLE);

            Bitmap bmp = ManagerUtils.generateKeysQRCode(this.manager);
            if (bmp!=null) ((ImageView)findViewById(R.id.qrCodeImage)).setImageBitmap(bmp);



        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void reset(View v){
        SharedPreferences mainActivity = getSharedPreferences("mainActivity", MODE_PRIVATE);
        mainActivity.edit().clear().apply();

        SharedPreferences managerActivity = getSharedPreferences("manager", MODE_PRIVATE);
        managerActivity.edit().clear().apply();


        (new File(getFilesDir(), "managerCertificate.ptasc")).delete();
        (new File(getFilesDir(), "managerECIESPubKey.ptasc")).delete();
        (new File(getFilesDir(), "managerECIESPrivKey.ptasc")).delete();
        (new File(getFilesDir(), "managerCertificatePublicKey.ptasc")).delete();
        (new File(getFilesDir(), "managerCertificatePrivKey.ptasc")).delete();
        (new File(getFilesDir(), "yubico.ptasc")).delete();

        if (manager!=null) manager.stopServices();

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        startActivity(intent);
    }

    public void append(View v){
        poolToBeAppended = ((EditText)findViewById(R.id.poolToBeAppended)).getText().toString();

        if (poolToBeAppended.equals("")){
            Toast.makeText(this,"Bad pool name",Toast.LENGTH_SHORT).show();
            return;
        }

        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("Scan the QR code containing manager keys");
        integrator.initiateScan();

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case OTP_REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    String otp = data.getStringExtra(OtpActivity.EXTRA_OTP);

                    File certificateFile = new File(getFilesDir(),"managerCertificate.ptasc");
                    if (manager.appendToOtherManagerPool(poolToBeAppended,CAECIESpubKey,CAECDSApubKey,otp,certificateFile)) {

                        Toast.makeText(this, "Success being appended to " + poolToBeAppended, Toast.LENGTH_SHORT).show();
                        appendingViewsEnabling(false);
                        cityPool = poolToBeAppended;
                        getSharedPreferences("manager", MODE_PRIVATE).edit().putString("appended", poolToBeAppended).apply();
                    }
                } else if (resultCode != Activity.RESULT_CANCELED && data != null) {
                    Throwable error = (Throwable)data.getSerializableExtra(OtpActivity.EXTRA_ERROR);
                    assert error != null;
                    Toast.makeText(this,error.getMessage(),Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

                if (result.getContents()!=null){
                    try {
                        JSONObject keys = new JSONObject(result.getContents());


                        CAECIESpubKey = (ECPublicKeyParameters) PublicKeyFactory.createKey(Base64.decode(keys.getString("CAECIES"),Base64.NO_WRAP));
                        CAECDSApubKey = KeyFactory.getInstance("EC").generatePublic(new X509EncodedKeySpec(Base64.decode(keys.getString("CAECDSA"),Base64.NO_WRAP)));

                        startActivityForResult(new Intent(this, OtpActivity.class), OTP_REQUEST_CODE);
                    } catch (JSONException | IOException | NoSuchAlgorithmException | InvalidKeySpecException e) {
                        manager.messageHandler.error(e,"Error processing QR code");
                    }
                }else manager.messageHandler.error(null,"Error processing QR code due to lack of information");
        }


    }



    void appendingViewsEnabling(boolean enableState){
        findViewById(R.id.poolToBeAppended).setEnabled(enableState);
        findViewById(R.id.provision).setEnabled(enableState);
    }

    void startViewsEnabling(boolean enabledMode){
        findViewById(R.id.poolManage).setEnabled(enabledMode);
        findViewById(R.id.otpID).setEnabled(enabledMode);
        findViewById(R.id.otpSecret).setEnabled(enabledMode);
        findViewById(R.id.start).setEnabled(enabledMode);
    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }



    public void master(View v){
        Intent i = new Intent(this,MasterActivity.class);
        MasterActivity.node = manager;
        MasterActivity.cityPool = cityPool;
        startActivity(i);
    }
}