package inconnu.android.datacollector.fragments;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;

import inconnu.android.datacollector.DataCollectorService;
import inconnu.android.datacollector.R;
import inconnu.anonymization.core.databases.dynamic.GeoIndistinguishabilityLaplaceAdaptativeEpsilon;
import inconnu.anonymization.core.databases.dynamic.GeoIndistinguishabilityLaplaceCluster;
import inconnu.anonymization.core.databases.dynamic.GeoIndistinguishabilityLaplaceSimple;
import inconnu.anonymization.core.databases.dynamic.utils.GeoLocationPoint;
import inconnu.ptasc.Configuration;

public class LocationFragment extends Fragment implements OnMapReadyCallback {

    GoogleMap googleMap;
    FusedLocationProviderClient mFusedLocationClient;
    LocationCallback mLocationCallback;

    MapView mapView;

    public static int selectedOption=-1;
    public static GeoIndistinguishabilityLaplaceSimple geoIndistinguishabilityLaplaceSimple;
    public static GeoIndistinguishabilityLaplaceCluster geoIndistinguishabilityLaplaceCluster;
    public static GeoIndistinguishabilityLaplaceAdaptativeEpsilon geoIndistinguishabilityLaplaceAdaptativeEpsilon;
    public static int frequency= 300;
    public static boolean isPublic=false;
    double s_r = 0;
    double s_l = 0;
    double c_r = 0;
    double c_l = 0;
    double a_r = 0;
    double a_l = 0;
    double a_a = 0;
    double a_d1 = 0;
    double a_b = 0;
    double a_d2= 0;

    Bundle d;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        d = savedInstanceState;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View inflate = inflater.inflate(R.layout.location_fragment, container, false);


        ((CheckBox)inflate.findViewById(R.id.publicCheck)).setOnCheckedChangeListener((buttonView, isChecked) -> isPublic = isChecked);



        inflate.findViewById(R.id.startBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inflate.findViewById(R.id.sc).setEnabled(false);
                Intent serviceIntent = new Intent(getContext(), DataCollectorService.class);
                getActivity().startForegroundService(serviceIntent);
            }
        });

        inflate.findViewById(R.id.stopBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inflate.findViewById(R.id.sc).setEnabled(true);
                Intent serviceIntent = new Intent(getContext(), DataCollectorService.class);
                getActivity().stopService(serviceIntent);
            }
        });



        ((EditText) inflate.findViewById(R.id.s_r)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("") && !s.toString().equals(".")) {
                    s_r = Double.parseDouble(s.toString());
                }else s_r=0;

                inflate.findViewById(R.id.s_switch).setEnabled(s_l != 0 && s_r != 0);

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ((EditText) inflate.findViewById(R.id.s_l)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("") && !s.toString().equals(".")) {
                    s_l = Double.parseDouble(s.toString());
                }else s_l=0;

                inflate.findViewById(R.id.s_switch).setEnabled(s_l != 0 && s_r != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        ((EditText) inflate.findViewById(R.id.c_r)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("") && !s.toString().equals(".")) {
                    c_r = Double.parseDouble(s.toString());
                }else c_r=0;


                inflate.findViewById(R.id.c_switch).setEnabled(c_l != 0 && c_r != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        ((EditText) inflate.findViewById(R.id.c_l)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("") && !s.toString().equals(".")) {
                    c_l = Double.parseDouble(s.toString());
                }else c_l=0;
                inflate.findViewById(R.id.c_switch).setEnabled(c_l != 0 && c_r != 0);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        ((SwitchCompat)inflate.findViewById(R.id.s_switch)).setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){


                geoIndistinguishabilityLaplaceSimple = new GeoIndistinguishabilityLaplaceSimple(s_r,s_l);

                ((SwitchCompat)inflate.findViewById(R.id.c_switch)).setChecked(false);
                selectedOption=0;
            }else{
                selectedOption = -1;
                geoIndistinguishabilityLaplaceSimple = null;
            }
        });

        ((SwitchCompat)inflate.findViewById(R.id.c_switch)).setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked){


                geoIndistinguishabilityLaplaceCluster = new GeoIndistinguishabilityLaplaceCluster(c_r,c_l);

                ((SwitchCompat)inflate.findViewById(R.id.s_switch)).setChecked(false);
                selectedOption=1;
            }else{
                selectedOption = -1;
                geoIndistinguishabilityLaplaceCluster = null;
            }
        });

        ((EditText)inflate.findViewById(R.id.frequency)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length()==0) frequency=300;
                else {
                    frequency = Integer.parseInt(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mapView= (MapView)inflate.findViewById(R.id.map);
        mapView.getMapAsync(this);
        mapView.onCreate(d);
        return inflate;
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mapView.onDestroy();
        selectedOption=-1;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mFusedLocationClient!=null) mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        mapView.onStop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;


        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());


        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);

                System.out.println(Configuration.PTASC_LOG + "DATA location received");
                Location lastLocation = locationResult.getLastLocation();
                GeoLocationPoint currentPoint = new GeoLocationPoint(lastLocation.getLatitude(), lastLocation.getLongitude());

                googleMap.clear();
                MarkerOptions mMarkerOptions = new MarkerOptions().position(new LatLng(currentPoint.latitude,currentPoint.longitude)).title("You are here!");
                googleMap.addMarker(mMarkerOptions);


                GeoLocationPoint anonPoint = null;
                switch (selectedOption){
                    case 0: //simple
                        anonPoint = geoIndistinguishabilityLaplaceSimple.sanitizePoint(new GeoLocationPoint(currentPoint.latitude, currentPoint.longitude));
                        break;
                    case 1: //cluster
                        anonPoint = geoIndistinguishabilityLaplaceCluster.sanitizeNewPoint(new GeoLocationPoint(currentPoint.latitude,currentPoint.longitude));
                        break;
                    case 2: //adaptative
                        anonPoint = geoIndistinguishabilityLaplaceAdaptativeEpsilon.sanitizeNewPoint(new GeoLocationPoint(currentPoint.latitude,currentPoint.longitude));
                        break;
                }

                if (anonPoint!=null) {
                    MarkerOptions anonMarkerOption = new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(120)).position(new LatLng(anonPoint.latitude, anonPoint.longitude)).title("Anonymized Position");
                    googleMap.addMarker(anonMarkerOption);


                    LatLngBounds latLngBounds = new LatLngBounds.Builder()
                            .include(new LatLng(currentPoint.latitude,currentPoint.longitude))
                            .include(new LatLng(anonPoint.latitude,anonPoint.longitude))
                            .build();
                    GeoLocationPoint gp1 = new GeoLocationPoint(latLngBounds.northeast.latitude,latLngBounds.northeast.longitude);
                    GeoLocationPoint gp2 = new GeoLocationPoint(latLngBounds.southwest.latitude,latLngBounds.southwest.longitude);

                    if (latLngBounds.northeast.equals(latLngBounds.southwest) || gp1.distanceTo(gp2)<200) {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngBounds.getCenter(), 15));
                    } else
                        googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds,30));
                }else{
                    googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentPoint.latitude,currentPoint.longitude),17));
                }

            }
        };


        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(),"The location permissions were not given",Toast.LENGTH_LONG).show();
            return;
        }else {
            getView().findViewById(R.id.startBtn).setEnabled(true);
            getView().findViewById(R.id.stopBtn).setEnabled(true);
            LocationRequest mLocationRequest = LocationRequest.create();
            mLocationRequest.setInterval(1000);
            mLocationRequest.setFastestInterval(1000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            mFusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.getMainLooper());
        }
    }


}
