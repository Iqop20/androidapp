package inconnu.android.datacollector.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.commons.lang3.tuple.Pair;
import org.bitlet.weupnp.GatewayDevice;
import org.bitlet.weupnp.GatewayDiscover;
import org.bitlet.weupnp.PortMappingEntry;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.ParserConfigurationException;

import fi.iki.elonen.NanoHTTPD;
import inconnu.android.datacollector.MasterActivity;
import inconnu.android.datacollector.R;
import inconnu.anonymization.core.databases.dynamic.utils.GeoLocationPoint;
import inconnu.ptasc.Configuration;
import inconnu.ptasc.PTascNode;
import inconnu.ptasc.Utils;
import inconnu.ptasc.communication.processors.OrionProcessorPTASC;

public class LocationGatherFragment extends Fragment  implements OnMapReadyCallback {

    static int LOCAL_PORT = 2000;
    static GatewayDevice d;
    View layout;
    LinearLayout subs_zone;

    static boolean isUPnPAvailable = false;
    static boolean allocationResult = false;
    static String externalIPAddress;

    static ScheduledFuture<?> scheduledFuture;

    static Semaphore accessSemaphore=new Semaphore(1);

    boolean isIdPattern;
    boolean isTypePattern;
    String type;
    String id;
    static Server.HTTP http;

    static Activity activity;

    ScheduledExecutorService executorService;


    Map<String,Pair<String, GeoLocationPoint>> subscriptions = new HashMap<>();

    PTascNode node = MasterActivity.node;

    MapView mapView;
    GoogleMap googleMap;
    Bundle bundle;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = savedInstanceState;
        new IsUPnPAvailable().execute();

        activity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        layout = inflater.inflate(R.layout.location_gather_fragment,container,false);


        ((EditText)layout.findViewById(R.id.id_text)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                id = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ((EditText)layout.findViewById(R.id.type_text)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                type = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ((CheckBox) layout.findViewById(R.id.id_check)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isIdPattern=isChecked;
            }
        });

        ((CheckBox) layout.findViewById(R.id.type_check)).setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isTypePattern=isChecked;
            }
        });

        ((Button)layout.findViewById(R.id.add_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generateSubscription();
            }
        });



        mapView= (MapView)layout.findViewById(R.id.map2);
        mapView.getMapAsync(this);
        mapView.onCreate(bundle);

        subs_zone = layout.findViewById(R.id.place);

        return layout;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.clear();


        scheduledFuture = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            try {
                System.out.println("DOING THIS");

                activity.runOnUiThread(() -> {
                    System.out.println("DOING THIS2");
                    googleMap.clear();

                    LinkedList<LatLng> points = new LinkedList<>();

                    try {
                        accessSemaphore.acquire();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    for (String sub : subscriptions.keySet()) {
                        Pair<String, GeoLocationPoint> data = subscriptions.get(sub);
                        String color = data.getLeft();
                        GeoLocationPoint point = data.getRight();
                        points.addLast(new LatLng(point.latitude, point.longitude));
                        CircleOptions circleOptions = new CircleOptions().radius(40).center(new LatLng(point.latitude, point.longitude)).fillColor(Color.parseColor(color)).visible(true).strokeColor(Color.parseColor(color));
                        googleMap.addCircle(circleOptions);
                    }

                    accessSemaphore.release();

                    if (points.size() > 1) {
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();

                        for (LatLng p : points) {
                            builder.include(p);
                        }

                        LatLngBounds latLngBounds = builder.build();

                        GeoLocationPoint gp1 = new GeoLocationPoint(latLngBounds.northeast.latitude,latLngBounds.northeast.longitude);
                        GeoLocationPoint gp2 = new GeoLocationPoint(latLngBounds.southwest.latitude,latLngBounds.southwest.longitude);

                        if (latLngBounds.northeast.equals(latLngBounds.southwest) || gp1.distanceTo(gp2)<200) {
                            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLngBounds.getCenter(), 15));
                        } else
                            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 10));


                    } else if (points.size() != 0) {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(points.get(0), 15));
                    }
                });


            } catch (Exception e) {
                System.out.println(e);
            }
        }, 0, 1, TimeUnit.SECONDS);
    }


    class IsUPnPAvailable extends AsyncTask<Void,Void,Void>{


        @Override
        protected Void doInBackground(Void... voids) {
            System.out.println("Starting weupnp");

            GatewayDiscover discover = new GatewayDiscover();
            System.out.println("Looking for Gateway Devices");
            try {
                discover.discover();
                d = discover.getValidGateway();

                if (null != d) {
                    System.out.println("Found gateway device.\n" + d.getModelName() + " " + d.getModelDescription());
                    isUPnPAvailable=true;
                } else {
                    System.out.println("No valid gateway device found.");
                    isUPnPAvailable=false;
                }
            } catch (IOException | SAXException | ParserConfigurationException e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            String s = "UPnP available: "+ isUPnPAvailable;
            ((TextView) layout.findViewById(R.id.upnp_available)).setText(s);

            if (isUPnPAvailable){
                new AllocatePort().execute();
            }

        }
    }

    class AllocatePort extends AsyncTask<Void,Void,Void>{

        Exception e=null;

        @Override
        protected Void doInBackground(Void... voids) {

            InetAddress localAddress = d.getLocalAddress();
            System.out.println("Using local address: " + localAddress);
            externalIPAddress = getExternalIp();
            System.out.println("External address: " + externalIPAddress);

            System.out.println("Attempting to map port " + LOCAL_PORT);
            PortMappingEntry portMapping = new PortMappingEntry();


            System.out.println("Querying device for port mapping: " + LOCAL_PORT);
            try {
                while (d.getSpecificPortMappingEntry(LOCAL_PORT, "TCP", portMapping) && LOCAL_PORT <= 65530) {
                    System.out.println("Port was already mapped");
                    LOCAL_PORT++;
                }


                if (!d.addPortMapping(LOCAL_PORT, LOCAL_PORT, localAddress.getHostAddress(), "TCP", node.name)) {
                    System.out.println("Port Failed to be Added");
                    throw new IOException("Port Failed to be Added");
                }else allocationResult = true;

            } catch (SAXException | IOException e) {
                this.e = e;
                allocationResult=false;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            if (e!=null){
                Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
            }else{
                new Thread(new Server()).start();

            }
            String s = "UPnP port forwarded: "+ allocationResult+ ", port: "+LOCAL_PORT+"  ";
            ((TextView) layout.findViewById(R.id.upnp_activate)).setText(s);
        }
    }

    public void generateSubscription(){
        JSONObject subscription=null;
        try {
            subscription = new JSONObject();
            JSONObject subject = new JSONObject();
            JSONArray entities = new JSONArray();
            JSONObject entity = new JSONObject();

            if (isIdPattern) {
                entity.put("idPattern", id);
            } else entity.put("id", id);

            if (isTypePattern) {
                entity.put("typePattern", type);
            } else entity.put("type", type);

            entities.put(entity);
            subject.put("entities", entities);
            subscription.put("subject", subject);


            JSONObject notification = new JSONObject();

            JSONObject url = new JSONObject();
            url.put("url", "http://" + externalIPAddress + ":" + LOCAL_PORT + "/");

            notification.put("http", url);


            subscription.put("notification", notification);

        } catch (JSONException e) {
            Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
            subscription=null;
        }
        System.out.println(subscription);

        if (subscription!=null) {
            new Subscribe().execute(subscription);
        }
    }

    class Subscribe extends AsyncTask<JSONObject,Void,Void>{

        Exception e;
        String data;
        int status=0;
        String subscriptionId=null;
        String color;
        @Override
        protected Void doInBackground(JSONObject... jsonObjects) {

            JSONObject subscription = jsonObjects[0];

            byte[] communication = node.communicate(node.endpointUSSN, Configuration.DEVICE_COMMUNICATION_PORT, OrionProcessorPTASC.processorName, OrionProcessorPTASC.input(subscription, "POST", "/v2/subscriptions"));
            JSONObject data;
            try {
                 data = new JSONObject(new String(communication));
            } catch (Exception e) {
                this.e = e;
                return null;
            }

            try {
                int status = data.getInt("code");
                String subscriptionId = data.getString("data").split("/v2/subscriptions/")[1];
                if (status==201){
                    Random obj = new Random();
                    int rand_num = obj.nextInt(0xffffff + 1);
// format it as hexadecimal string and print
                    color = String.format("#%06x", rand_num);


                    accessSemaphore.acquire();

                    subscriptions.put(subscriptionId,Pair.of(color,new GeoLocationPoint(0,0)));

                    accessSemaphore.release();

                    this.subscriptionId = subscriptionId;
                }else{
                    this.data =subscriptionId;
                    this.status =  status;
                }

            } catch (JSONException | InterruptedException e) {
                this.e = e;
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            if (e != null) {
                Toast.makeText(getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
            }else if (status!=0){
                Toast.makeText(getContext(),"Error: code="+status+", data="+data,Toast.LENGTH_LONG).show();
            }else{
                //Append the subscription to the visual environment

                View sub = getLayoutInflater().inflate(R.layout.location_gather_item, subs_zone, false);
                ((TextView)sub.findViewById(R.id.subName)).setText(subscriptionId);
                ((Button)sub.findViewById(R.id.cancel_sub)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        new UnSubscribe().execute(subscriptionId);
                    }
                });
                sub.findViewById(R.id.subColor).setBackgroundColor(Color.parseColor(color));

                subs_zone.addView(sub);
            }
        }
    }


    class UnSubscribeAll extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... voids) {

            for (String subId : subscriptions.keySet()) {
                node.communicate(node.endpointUSSN, Configuration.DEVICE_COMMUNICATION_PORT, OrionProcessorPTASC.processorName, OrionProcessorPTASC.input(new JSONObject(), "DELETE", "/v2/subscriptions/" + subId));
            }
            return null;
        }
    }

    class UnSubscribe extends AsyncTask<String,Void,Void>{

        @Override
        protected Void doInBackground(String... strings) {

            String subId = strings[0];

            byte[] delete = node.communicate(node.endpointUSSN, Configuration.DEVICE_COMMUNICATION_PORT, OrionProcessorPTASC.processorName, OrionProcessorPTASC.input(new JSONObject(), "DELETE", "/v2/subscriptions/" + subId));
            try {
                accessSemaphore.acquire();
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
            if (delete!=null) subscriptions.remove(subId);
            accessSemaphore.release();
            return null;
        }

        @Override
        protected void onPostExecute(Void unused) {
            subs_zone.removeAllViews();

            try {
                accessSemaphore.acquire();
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
            for(String subscriptionId : subscriptions.keySet()){

                String color = subscriptions.get(subscriptionId).getLeft();

                View sub = getLayoutInflater().inflate(R.layout.location_gather_item, subs_zone, false);
                ((TextView)sub.findViewById(R.id.subName)).setText(subscriptionId);
                ((Button)sub.findViewById(R.id.cancel_sub)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        subscriptions.remove(subscriptionId);
                        new UnSubscribe().execute(subscriptionId);
                    }
                });
                sub.findViewById(R.id.subColor).setBackgroundColor(Color.parseColor(color));

                subs_zone.addView(sub);
            }
            accessSemaphore.release();
        }
    }





    class Server implements Runnable{

        @Override
        public void run() {

            try {

                http = new HTTP("0.0.0.0", LOCAL_PORT);

                http.start();
                System.out.println("Server Running on port: "+LOCAL_PORT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        class HTTP extends NanoHTTPD{

            public HTTP(String hostname, int port) {
                super(hostname, port);
            }

            @Override
            public Response serve(IHTTPSession session) {
                try {

                    String s = session.getHeaders().get("content-length");

                    String recved = new String(Utils.readBytesFromInputStream(session.getInputStream(),Integer.parseInt(s)));
                    JSONObject notification = new JSONObject(recved);

                    //TODO: Do Something with that
                    String subscriptionId = notification.getString("subscriptionId");
                    double latitude = notification.getJSONArray("data").getJSONObject(0).getJSONObject("latitude").getDouble("value");
                    double longitude = notification.getJSONArray("data").getJSONObject(0).getJSONObject("longitude").getDouble("value");

                    System.out.println(notification);

                    accessSemaphore.acquire();
                    if (subscriptions.containsKey(subscriptionId)) {
                        Pair<String, GeoLocationPoint> stringGeoLocationPointPair = subscriptions.get(subscriptionId);
                        subscriptions.put(subscriptionId, Pair.of(stringGeoLocationPointPair.getLeft(), new GeoLocationPoint(latitude, longitude)));
                    }
                    accessSemaphore.release();
                    return newFixedLengthResponse(Response.Status.OK,"text/plain","received");
                } catch (IOException | JSONException | InterruptedException e) {
                    e.printStackTrace();
                }
                return newFixedLengthResponse(Response.Status.INTERNAL_ERROR,"text/plain","Error parsing JSON");
            }

        }


    }



    @Override
    public void onStop() {
        super.onStop();
        new UnSubscribeAll().execute();
        (new AsyncTask<Void,Void,Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    d.deletePortMapping(LOCAL_PORT, "TCP");
                } catch (IOException | SAXException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }).execute();
        if (http!=null) http.stop();
        mapView.onStop();
        if (scheduledFuture!=null) scheduledFuture.cancel(true);
    }


    static String getExternalIp(){
        try {
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));

            return in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
