package inconnu.android.datacollector;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.text.HtmlCompat;

public class MainActivity extends AppCompatActivity {

    final int SHUTDOWN_OPTMIZATIONS=1;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        ((TextView)findViewById(R.id.description)).setText(HtmlCompat.fromHtml("<b>Choose one of the modes</b><br/>This cannot be changed unless you reset the APP",HtmlCompat.FROM_HTML_MODE_COMPACT));
        ((Button) findViewById(R.id.lc)).setText(HtmlCompat.fromHtml("<b>Client</b><br/><br/>Be able to send data to Fiware ORION",HtmlCompat.FROM_HTML_MODE_COMPACT));
        ((Button) findViewById(R.id.lm)).setText(HtmlCompat.fromHtml("<b>Manager</b><br/><br/>Be able to manage a pool of client devices and send data to Fiware ORION",HtmlCompat.FROM_HTML_MODE_COMPACT));



    }


    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = new Intent();
        String packageName = getPackageName();
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        if (!pm.isIgnoringBatteryOptimizations(packageName)) {
            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent,SHUTDOWN_OPTMIZATIONS);
        }else {

            SharedPreferences mainActivity = getSharedPreferences("mainActivity", MODE_PRIVATE);
            String mode = mainActivity.getString("mode", "none");
            switch (mode) {
                case "client":
                    launchIntent(ClientActivity.class, "client", true);
                    break;
                case "manager":
                    launchIntent(ManagerActivity.class, "manager", true);
                    break;
            }
        }
    }

    public void launchClient(View v){
        launchIntent(ClientActivity.class,"client",false);
    }
    public void launchManager(View v){
        launchIntent(ManagerActivity.class,"manager",false);
    }

    void launchIntent(Class<?> activityClass,String name,boolean fromOnStart){
        Intent intent = new Intent(this, activityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        if (!fromOnStart) {
            SharedPreferences mainActivity = getSharedPreferences("mainActivity", MODE_PRIVATE);
            SharedPreferences.Editor edit = mainActivity.edit();
            edit.putString("mode", name);
            edit.apply();
        }

        startActivity(intent);
        overridePendingTransition(0, 0);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case SHUTDOWN_OPTMIZATIONS:
                if (resultCode!= Activity.RESULT_OK){
                    this.finish();
                }else {

                    SharedPreferences mainActivity = getSharedPreferences("mainActivity", MODE_PRIVATE);
                    String mode = mainActivity.getString("mode", "none");
                    switch (mode) {
                        case "client":
                            launchIntent(ClientActivity.class, "client", true);
                            break;
                        case "manager":
                            launchIntent(ManagerActivity.class, "manager", true);
                            break;
                    }
                }
                break;
        }
    }
}
