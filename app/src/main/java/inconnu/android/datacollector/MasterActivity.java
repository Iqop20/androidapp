package inconnu.android.datacollector;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.Task;

import inconnu.android.datacollector.fragments.LocationFragment;
import inconnu.android.datacollector.fragments.LocationGatherFragment;
import inconnu.ptasc.PTascNode;

public class MasterActivity extends AppCompatActivity {

    private static final int SERVICE_PERMISSIONS = 1;
    private static final int GPS_ACTIVATION_REQUEST_RESPONSE = 2;
    public static PTascNode node;
    public static String cityPool;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master);

        locationAnon(null);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case SERVICE_PERMISSIONS:
                boolean granted = true;
                for (int res : grantResults){
                    if (granted){
                        granted = (res==PackageManager.PERMISSION_GRANTED);
                    }
                }
                if (granted){
                    locationAnon(null);
                }else {
                    Toast.makeText(this,"Failed: Make sure to accept all the permissions",Toast.LENGTH_LONG).show();
                }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case GPS_ACTIVATION_REQUEST_RESPONSE:
                if (resultCode == Activity.RESULT_OK){
                    locationAnon(null);
                }else Toast.makeText(this,"Unable to launch location due to lack of location activation",Toast.LENGTH_LONG).show();
                break;
        }
    }

    public void locationAnon(View v){


        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                requestPermissions(new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, SERVICE_PERMISSIONS);
            }else requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, SERVICE_PERMISSIONS);
        }else {

            LocationRequest mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            LocationSettingsRequest.Builder locationSettingsBuilder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);

            Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(this).checkLocationSettings(locationSettingsBuilder.build());
            result.addOnCompleteListener(task -> {
                boolean requiredResolution=false;
                try {
                    task.getResult(ApiException.class);
                }catch (ApiException e){

                    switch (e.getStatusCode()){
                        case (LocationSettingsStatusCodes.RESOLUTION_REQUIRED):
                            try{
                                requiredResolution=true;
                                ResolvableApiException resolvableApiException = (ResolvableApiException) e;
                                resolvableApiException.startResolutionForResult(this,GPS_ACTIVATION_REQUEST_RESPONSE);
                            } catch (IntentSender.SendIntentException sendIntentException) {
                                sendIntentException.printStackTrace();
                            }
                            break;
                        case (LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE):
                            break;
                    }
                }

                if (!requiredResolution){
                    ((TextView)findViewById(R.id.title)).setText("Location Sharing");
                    getSupportFragmentManager().beginTransaction()
                            .setReorderingAllowed(true)
                            .replace(R.id.fragment_container_view, LocationFragment.class,null)
                            .commit();
                }
            });
        }







    }

    public void locationGather(View view) {
        ((TextView)findViewById(R.id.title)).setText("Location Gather");
        getSupportFragmentManager().beginTransaction()
                .setReorderingAllowed(true)
                .replace(R.id.fragment_container_view, LocationGatherFragment.class,null)
                .commit();
    }
}
