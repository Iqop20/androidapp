package inconnu.android.datacollector;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Toast;

import org.bouncycastle.crypto.params.ECPublicKeyParameters;

import java.io.File;
import java.security.PublicKey;

import inconnu.ptasc.client.Client;

public class ClientUtils {

    public static class LaunchClient extends AsyncTask<Object,String, Client>{

        Exception e;
        Activity context;

        @Override
        protected Client doInBackground(Object... objects) {

            context = (Activity) objects[0];
            String name = (String) objects[1];
            ECPublicKeyParameters CAECIESpubKey = (ECPublicKeyParameters) objects[2];
            PublicKey CAECDSApubKey = (PublicKey) objects[3];
            String otp = (String) objects[4];
            String pool = (String) objects[5];
            String cityName = (String) objects[6];

            File certificateFile = new File(context.getFilesDir(),name+"Certificate.ptasc");
            File certificatePublicKeyFile = new File(context.getFilesDir(),name+"CertificatePublicKey.ptasc");
            File certificatePrivKeyFile = new File(context.getFilesDir(),name+"CertificatePrivKey.ptasc");

            try {
               return new Client(name, CAECIESpubKey, CAECDSApubKey, pool, "endpoint."+cityName,otp, certificateFile, certificatePublicKeyFile, certificatePrivKeyFile,new AndroidMessageHandler(context),"endpoint."+cityName,true);
            } catch (Exception e) {
                this.e = e;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Client client) {
            if (client==null){
                Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
            }else Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show();
        }
    }


    public static int convertToInt(byte b){
        return (Byte.valueOf(b)).intValue() & 0xFF;
    }


}
