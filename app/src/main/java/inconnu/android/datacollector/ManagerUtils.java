package inconnu.android.datacollector;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Base64;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;


import org.bouncycastle.crypto.util.SubjectPublicKeyInfoFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import inconnu.ptasc.manager.Manager;

public class ManagerUtils {

    public static Bitmap generateKeysQRCode(Manager manager){
        try {

            QRCodeWriter qrCodeWriter = new QRCodeWriter();

            String ecdsaEncoded = Base64.encodeToString(manager.ECDSApubKey.getEncoded(), Base64.NO_WRAP);
            String eciesEncoded = Base64.encodeToString(SubjectPublicKeyInfoFactory.createSubjectPublicKeyInfo(manager.ECIESpubKey).getEncoded(), Base64.NO_WRAP);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("CAECDSA",ecdsaEncoded);
            jsonObject.put("CAECIES",eciesEncoded);



            BitMatrix bitMatrix = qrCodeWriter.encode(jsonObject.toString(), BarcodeFormat.QR_CODE, 300, 300);
            int height = bitMatrix.getHeight();
            int width = bitMatrix.getWidth();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++){
                for (int y = 0; y < height; y++){
                    bmp.setPixel(x, y, bitMatrix.get(x,y) ? Color.BLACK : Color.WHITE);
                }
            }

            return bmp;
        } catch (WriterException | IOException | JSONException e) {
            manager.messageHandler.error(e,"Error generating QR code");
        }
        return null;
    }
}
